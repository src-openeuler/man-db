%global cache /var/cache/man

Name:            man-db
Version:         2.13.0
Release:         1
Summary:         Online database for using man pages
License:         GPL-2.0-or-later AND GPL-3.0-or-later
URL:             https://www.nongnu.org/man-db/
Source0:         https://download.savannah.gnu.org/releases/%{name}/%{name}-%{version}.tar.xz
Source1:         man-db.crondaily
Source2:         man-db.sysconfig
Source3:         man-db-cache-update.service
Source4:         man-db-restart-cache-update.service

BuildRequires:   gcc systemd gdbm-devel gettext groff >= 1.21 less libpipeline-devel zlib-devel
BuildRequires:   po4a perl-interpreter perl-version make chrpath pkgconfig(libseccomp)
Requires:        coreutils grep groff-base gzip less crontabs
Requires(post):  coreutils

Provides:        man = %{version} man-pages-reader = %{version} bundled(gnulib) = 20140202
Obsoletes:       man < 2.0
Provides:        man-db-cron
Obsoletes:       man-db-cron
Obsoletes:       man-pages-zh-CN

%description
man-db is an implementation of the standard Unix documentation system accessed using the man command.
It uses a Berkeley DB database in place of the traditional flat-text whatis databases.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure \
    --with-sections="1 1p 8 2 3 3p 4 5 6 7 9 0p n l p o 1x 2x 3x 4x 5x 6x 7x 8x" \
    --disable-setuid --disable-cache-owner --with-browser=elinks --with-lzip=lzip
%make_build

%check
%make_build check

%install
%make_install
%delete_la

# move the documentation to the relevant place
mv %{buildroot}%{_datadir}/doc/man-db/* ./

# remove zsoelim man page - part of groff package
rm $RPM_BUILD_ROOT%{_datadir}/man/man1/zsoelim.1

# install cache directory
install -d -m 0755 %{buildroot}/var/cache/man

# install cron script for man-db creation/update
mkdir -p %{buildroot}%{_sysconfdir}/cron.daily
install -D -p -m 0755 %{S:1} %{buildroot}%{_sysconfdir}/cron.daily/man-db.cron

# config for cron script
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -D -p -m 0644 %{S:2} %{buildroot}%{_sysconfdir}/sysconfig/man-db

# config for tmpfiles.d
install -D -p -m 0644 init/systemd/man-db.conf %{buildroot}/usr/lib/tmpfiles.d/.

# man-db-cache-update.service and man-db-restart-cache-update.service
install -D -p -m 0644 %{S:3} %{buildroot}%{_unitdir}/man-db-cache-update.service
install -D -p -m 0644 %{S:4} %{buildroot}%{_unitdir}/man-db-restart-cache-update.service

%find_lang %{name}
%find_lang %{name}-gnulib

chrpath -d %{buildroot}%{_sbindir}/accessdb
chrpath -d %{buildroot}%{_libexecdir}/%{name}/*
chrpath -d %{buildroot}%{_bindir}/{*man*,whatis,lexgrog}
chrpath -d %{buildroot}%{_libdir}/%{name}/libmandb-%{version}.so
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
echo "%{_libdir}/man-db" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
# clear the old cache
%{__rm} -rf %{cache}/* || :
/sbin/ldconfig

# update cache
%transfiletriggerin -- %{_mandir}
if [ -x /usr/bin/systemd-run -a -x /usr/bin/systemctl ]; then
    /usr/bin/systemd-run /usr/bin/systemctl start man-db-cache-update >/dev/null 2>&1 || :
fi

# update cache
%transfiletriggerpostun -- %{_mandir}
if [ -x /usr/bin/systemd-run -a -x /usr/bin/systemctl ]; then
    /usr/bin/systemd-run /usr/bin/systemctl start man-db-cache-update >/dev/null 2>&1 || :
fi

%postun
/sbin/ldconfig

%files -f %{name}.lang -f %{name}-gnulib.lang
%license COPYING
%doc README.md man-db-manual.txt man-db-manual.ps ChangeLog NEWS.md
%config(noreplace) %{_sysconfdir}/man_db.conf
%config(noreplace) %{_sysconfdir}/sysconfig/man-db
%config(noreplace) /usr/lib/tmpfiles.d/man-db.conf
%config(noreplace) %{_sysconfdir}/cron.daily/man-db.cron
%config(noreplace) /etc/ld.so.conf.d/*
%{_unitdir}/man-db-cache-update.service
%{_unitdir}/man-db-restart-cache-update.service
%{_sbindir}/accessdb
%{_bindir}/man
%{_bindir}/whatis
%{_bindir}/apropos
%{_bindir}/manpath
%{_bindir}/lexgrog
%{_bindir}/catman
%{_bindir}/mandb
%{_bindir}/man-recode
%dir %{_libdir}/man-db
%{_libdir}/man-db/*.so
%dir %{_libexecdir}/man-db
%{_libexecdir}/man-db/globbing
%{_libexecdir}/man-db/manconv
%{_libexecdir}/man-db/zsoelim
/usr/lib/systemd/system/man-db.service
/usr/lib/systemd/system/man-db.timer
%verify(not mtime) %dir %{cache}
%lang(da)       %{_datadir}/man/da/man*/*
%lang(de)       %{_datadir}/man/de/man*/*
%lang(es)       %{_datadir}/man/es/man*/*
%lang(fr)       %{_datadir}/man/fr/man*/*
%lang(id)       %{_datadir}/man/id/man*/*
%lang(it)       %{_datadir}/man/it/man*/*
%lang(ja)       %{_datadir}/man/ja/man*/*
%lang(ko)       %{_datadir}/man/ko/man*/*
%lang(nl)       %{_datadir}/man/nl/man*/*
%lang(pl)       %{_datadir}/man/pl/man*/*
%lang(pt)       %{_datadir}/man/pt/man*/*
%lang(pt_BR)    %{_datadir}/man/pt_BR/man*/*
%lang(ru)       %{_datadir}/man/ru/man*/*
%lang(ro)       %{_datadir}/man/ro/man*/*
%lang(sr)       %{_datadir}/man/sr/man*/*
%lang(sv)       %{_datadir}/man/sv/man*/*
%lang(tr)       %{_datadir}/man/tr/man*/*
%lang(zh_CN)    %{_datadir}/man/zh_CN/man*/*
%{_mandir}/man1/apropos.1*
%{_mandir}/man1/lexgrog.1*
%{_mandir}/man1/man.1*
%{_mandir}/man1/manconv.1*
%{_mandir}/man1/manpath.1*
%{_mandir}/man1/whatis.1*
%{_mandir}/man1/man-recode.1*
%{_mandir}/man5/manpath.5*
%{_mandir}/man8/accessdb.8*
%{_mandir}/man8/catman.8*
%{_mandir}/man8/mandb.8*

%changelog
* Fri Aug 30 2024 Funda Wang <fundawang@yeah.net> - 2.13.0-1
- update to 2.13.0
- schedule interrupted cache update for the next boot (rhbz#1678464)

* Fri Jul  5 2024 warlcok <hunan@kylinos.cn> - 2.12.1-1
- upgrade to 2.12.1

* Thu Mar 28 2024 lilong <lilong@kylinos.cn> - 2.11.2-2
- revert "Fix crash when mandoc install"

* Wed Feb 28 2024 gengqihu <gengqihu2@h-partners.com> - 2.11.2-1
- revert "upgrade version to 2.12.0"

* Tue Jan 16 2024 renhongxun <renhongxun@h-partners.com> - 2.12.0-1
- upgrade version to 2.12.0

* Tue Jul 25 2023 renhongxun <renhongxun@h-partners.com> - 2.11.2-1
- upgrade version to 2.11.2

* Fri Jun 30 2023 lilong <lilong@kylinos.cn> - 2.11.0-3
- Fix crash when mandoc install

* Sat Jun 17 2023 yanglongkang <yanglongkang@h-partners.com> - 2.11.0-2
- Fix crash when processing stray cats

* Mon Nov 07 2022 renhongxun <renhongxun@h-partners.com> - 2.11.0-1
- upgrade version to 2.11.0

* Wed Oct 19 2022 shixuantong <shixuantong1@huawei.com> - 2.9.4-3
- sync changes from openEuler-22.03-LTS-Next to solve downgrade issues

* Sat Nov 27 2021 liudabo <liudabo1@huawei.com> - 2.9.4-1
- upgrade version to 2.9.4

* Fri Sep 17 2021 wuchaochao <wuchaochao4@huawei.com> - 2.9.3-6
- add BuildRequires chrpath

* Wed Sep 8 2021 wuchaochao <wuchaochao4@huawei.com> - 2.9.3-5
- add so path to /etc/ld.so.conf.d when del rpath

* Sat Sep 4 2021 wuchaochao <wuchaochao4@huawei.com> - 2.9.3-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Remove rpath compilation option

* Tue Aug 17 2021 Jianmin <jianmin@iscas.ac.cn> - 2.9.3-3
- Type:NA
- ID:NA
- SUG:NA
- DESC:change dependency on groff to groff-base

* Wed Nov 25 2020 shixuantong <shixuantong@huawei.com> - 2.9.3-2
- Type:NA
- ID:NA
- SUG:NA
- DESC:Delete unnecessary installation dependencies

* Thu Jul 16 2020 shixuantong <shixuantong@huawei.com> - 2.9.3-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 2.9.3-1

* Mon Feb 17 2020 chengquan <chengquan3@huawei.com> - 2.8.7-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add necessary BuildRequire

* Mon Dec 16 2019 chengquan <chengquan3@huawei.com> - 2.8.7-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove help package into main package

* Tue Sep 24 2019 chengquan <chengquan3@huawei.com> - 2.8.7-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revise requires of groff

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.7-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix error in POSTIN scriptlet

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.7-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update software

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.4-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add global marco

* Tue Sep 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8.4-2
- Package init
